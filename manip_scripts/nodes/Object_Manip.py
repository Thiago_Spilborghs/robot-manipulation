#!/usr/bin/env python

import smach_ros
import smach
from smach import state
import rospy
from std_msgs.msg import String, Float64
from dynamixel_msgs.msg import JointState
import ArmMod
import numpy as np
import pyglet
import math
import time
from dynamixel_controllers.srv import SetSpeed


class GoTo():

    def __init__(self, x, y, z):
        # Publishers
        self.joint1 = rospy.Publisher('/tilt2_controller/command', Float64, queue_size=20)  #Junta 1
        self.joint2 = rospy.Publisher('/tilt3_controller/command', Float64, queue_size=20)  #Junta 2
        self.joint3 = rospy.Publisher('/tilt4_controller/command', Float64, queue_size=20)  #Junta 3
        self.joint4 = rospy.Publisher('/tilt5_controller/command', Float64, queue_size=20)  #Pulso
        self.base = rospy.Publisher('/tilt_controller/command', Float64, queue_size=20)  #base

        # Subscribers
        rospy.Subscriber('/tilt2_controller/state', JointState, self.callback1, queue_size=1)  #callback para receber dados do servo [id:6]
        rospy.Subscriber('/tilt3_controller/state', JointState, self.callback2, queue_size=1)  #callback para receber dados do servo [id:6]
        rospy.Subscriber('/tilt4_controller/state', JointState, self.callback3, queue_size=1)  #callback para receber dados do servo [id:6]

        rate = rospy.Rate(5)
        self.count = 0
        self.error_default = 0.2
        self.diff_vel = [0, 0, 0]
        self.speeds = [0, 0, 0]
        # self.joints_values = ['joint2_speed','joint3_speed','joint4_speed']
        self.coordX = x
        self.coordY = y
        self.coordZ = z
        self.velocity_lim = 0.5
        self.default_joint_speed = 0.3
        self.angle = np.array([math.pi / 2, -math.pi / 2, 0, math.pi / 2])
        self.servo_speed = dict()
        self.controller = ['tilt2_controller', 'tilt3_controller', 'tilt4_controller']
        # for x in (self.controller):
        #     self.joint_position[x] =rospy.Publisher('/'+x+'/command',Float64,queue_size=5)
        if self.execute() == 'in_progress':
            self.execute()

    def callback1(self, data):
        self.pos_joint2 = data.current_pos
        self.error_joint2 = data.error
        self.moving_joint2 = data.is_moving

    def callback2(self, data):
        self.pos_joint3 = data.current_pos
        self.error_joint3 = data.error
        self.moving_joint3 = data.is_moving

    def callback3(self, data):
        self.pos_joint4 = data.current_pos
        self.error_joint4 = data.error
        self.moving_joint4 = data.is_moving

    def execute(self):
        rospy.loginfo('Moving to point')
        time.sleep(0.3)

        for x in (self.controller):
            set_speed_service = '/' + x + '/set_speed'
            rospy.wait_for_service(set_speed_service)
            self.servo_speed[x] = rospy.ServiceProxy(set_speed_service, SetSpeed, persistent=True)
            self.servo_speed[x](self.default_joint_speed)

        # Current position
        self.current_pose_joint1 = self.pos_joint2
        self.current_pose_joint2 = self.pos_joint3
        self.current_pose_joint3 = self.pos_joint4

        self.q_values = [
            np.round(self.current_pose_joint1 + (1.98), 2),
            np.round(self.current_pose_joint2 - (0.41), 2),
            np.round(self.current_pose_joint3 - (0.46), 2), math.pi / 2
        ]

        # Create a Arm3Link
        arm = ArmMod.Arm3Link(q=self.q_values, q0=self.q_values, L=np.array([130, 133, 225]))
        arm.q = arm.inv_kin(xyz=[self.coordX, self.coordY, self.coordZ])

        if not (math.isnan(arm.q[0])):
            #transformations
            q1 = np.round(arm.q[0] - (1.98), 2)
            q2 = np.round(arm.q[1] + (0.41), 2)
            q3 = np.round(arm.q[2] + (0.46), 2)
            q4 = np.round(arm.q[3] + (-0.71), 2)

            # Diff os joint angles -> now to target

            self.diff_vel[0] = abs(abs(self.current_pose_joint1) - abs(q1))
            self.diff_vel[1] = abs(abs(self.current_pose_joint2) - abs(q2))
            self.diff_vel[2] = abs(abs(self.current_pose_joint3) - abs(q3))

            #reorganizando a lista de diferencas

            for x in range(len(self.diff_vel) - 1, 0, -1):
                for i in range(x):
                    if self.diff_vel[i] > self.diff_vel[i + 1]:
                        temp = self.diff_vel[i]
                        temp_aux = self.controller[i]
                        self.diff_vel[i] = self.diff_vel[i + 1]
                        self.controller[i] = self.controller[i + 1]
                        self.diff_vel[i + 1] = temp
                        self.controller[i + 1] = temp_aux

            #Making the proportion values of speeds
            self.speeds = np.round(
                [
                    (((self.diff_vel[0] * 100) / self.diff_vel[2]) / 100) * self.velocity_lim,
                    (((self.diff_vel[1] * 100) / self.diff_vel[2]) / 100) * self.velocity_lim, self.velocity_lim
                ], 1
            )

            for x in range(len(self.speeds)):
                if self.speeds[x] == 0.0:
                    self.speeds[x] = 0.1
                self.servo_speed[self.controller[x]](self.speeds[x])
                rospy.loginfo("\nSPEEDS: %s  Joint: %s \n", str(self.speeds[x]), str(self.controller[x]))

            #Publishing joint values

            self.joint1.publish(q1)
            # time.sleep(0.1)
            self.joint2.publish(q2)
            self.joint3.publish(q3)
            self.base.publish(q4)
            time.sleep(3)

            while (self.moving_joint2 == True and self.moving_joint3 == True and self.moving_joint4 == True):
                rospy.loginfo('Moving to point')

            # Errors
            rospy.loginfo(
                '\n Error joint2: %f \n Error joint3: %f \n Error joint4: %f', np.absolute(self.error_joint2), np.absolute(self.error_joint3),
                np.absolute(self.error_joint4)
            )

        if np.absolute(self.error_joint2) < self.error_default and np.absolute(self.error_joint3
            ) < self.error_default and np.absolute(self.error_joint4) < self.error_default:
            rospy.loginfo('Goal reached')
            self.count = 0
            exit()
        elif self.count < 2:
            rospy.loginfo('Sending Goal Again')
            self.count += 1
            time.sleep(0.1)
            return 'in_progress'
        elif (math.isnan(arm.q[0])):
            rospy.loginfo("NAN VALUE ERROR !!!")
            return 'ok'
        else:
            return 'ok'
