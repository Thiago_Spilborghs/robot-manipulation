#!/usr/bin/env python

import rospy
import numpy
import time
from std_msgs.msg import String

class Pimba():
    def __init__(self):
        self.pub = rospy.Publisher('/Manipulation',String,queue_size=10)
        self.execute()

    def execute(self):
        while True:
            time.sleep(1)
            print 'ok'
            self.pub.publish('wave')

if __name__ == '__main__':
    rospy.init_node('show')
    Pimba()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        rospy.loginfo('Shutting down node test')
