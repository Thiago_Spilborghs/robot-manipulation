#!/usr/bin/env python

import rospy
import yaml
import os.path
import time
import numpy as np
from std_msgs.msg import Float64
from dynamixel_msgs.msg import JointState
from dynamixel_controllers.srv import TorqueEnable, SetSpeed

class gest():
    def __init__(self,name):

        # Services
        self.srv_joint_1 = rospy.ServiceProxy('/tilt2_controller/torque_enable', TorqueEnable, persistent=True)
        self.srv_joint_2 = rospy.ServiceProxy('/tilt3_controller/torque_enable', TorqueEnable, persistent=True)
        self.srv_joint_3 = rospy.ServiceProxy('/tilt4_controller/torque_enable', TorqueEnable, persistent=True)
        self.srv_wrist = rospy.ServiceProxy('/tilt5_controller/torque_enable', TorqueEnable, persistent=True)
        self.srv_base = rospy.ServiceProxy('/tilt_controller/torque_enable', TorqueEnable, persistent=True)

        # Subscribers
        rospy.Subscriber('/tilt2_controller/state', JointState, self.callback_joint_1, queue_size=1)
        rospy.Subscriber('/tilt3_controller/state', JointState, self.callback_joint_2, queue_size=1)
        rospy.Subscriber('/tilt4_controller/state', JointState, self.callback_joint_3, queue_size=1)
        rospy.Subscriber('/tilt5_controller/state', JointState, self.callback_wrist, queue_size=1)
        rospy.Subscriber('/tilt_controller/state', JointState, self.callback_base, queue_size=1)
        #Publishers
        self.joint1 = rospy.Publisher('/tilt2_controller/command', Float64, queue_size=1)  #Junta 1
        self.joint3 = rospy.Publisher('/tilt4_controller/command', Float64, queue_size=1)  #Junta 3
        self.joint2 = rospy.Publisher('/tilt3_controller/command', Float64, queue_size=1)  #Junta 2
        self.joint4 = rospy.Publisher('/tilt5_controller/command', Float64, queue_size=1)  #Pulso
        self.base = rospy.Publisher('/tilt_controller/command', Float64, queue_size=1)
        self.position_joint_1 = 0
        self.position_joint_2 = 0
        self.position_joint_3 = 0
        self.position_wrist = 0
        self.position_base = 0
        self.moving = [0, 0, 0, 0, 0]
        self.vel_max = 0.6
        self.name = name
        self.execute()

    def callback_joint_1(self,data):
        self.position_joint_1 = data.current_pos
        self.moving[0] = data.velocity


    def callback_joint_2(self,data):
        global position_joint_2, moving
        position_joint_2 = data.current_pos
        moving[1] = data.velocity


    def callback_joint_3(self,data):
        global position_joint_3, moving
        position_joint_3 = data.current_pos
        moving[2] = data.velocity


    def callback_wrist(self,data):
        global position_wrist, moving
        position_wrist = data.current_pos
        moving[3] = data.velocity


    def callback_base(self,data):
        global position_base, moving
        position_base = data.current_pos
        moving[4] = data.velocity


    def execute(self):
        # Giving global scope to servo position variables

        # Reading yaml file for gesture
        fname = os.path.expanduser('~') + '/catkin_ws/src/robot_manip/manip_launch/config/gestures.yaml'
        stream = open(fname, 'r')
        data = yaml.load(stream)
        keys = list(data.keys())

        # Calling a name for gesture
        gesture_name = str(self.name)
        if not gesture_name:
            rospy.logerr('It\'s required name for gesture ... ')
        else:
            self.srv_joint_1(True)
            self.srv_joint_2(True)
            self.srv_joint_3(True)
            self.srv_wrist(True)
            self.srv_base(True)
            #t = time.getime.now()
            for x in data[gesture_name]:
                rospy.loginfo('Playing ...')
                rospy.sleep(0.2)  #record a point every step of 600 ms
                self.joint1.publish(x[0])
                self.joint2.publish(x[1])
                self.joint3.publish(x[2])
                self.joint4.publish(x[3])
                self.base.publish(x[4])
                #if time.now() - t >= 10:
                #    break

        exit()

        # rospy.loginfo('FINISHED!!!')
