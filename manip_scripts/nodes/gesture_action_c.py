#!/usr/bin/env python

import rospy
import yaml
import os.path
import time
import numpy as np
import actionlib
from std_msgs.msg import Float64
from dynamixel_msgs.msg import JointState
from dynamixel_controllers.srv import TorqueEnable, SetSpeed
from manip_scripts.msg import manip_action_msgAction, manip_action_msgFeedback, manip_action_msgResult, manip_action_msgGoal


PENDING = 0
ACTIVE = 1
DONE = 2
WARN = 3
ERROR = 4

gesture_name = 'open_grip'

def feedback_callback (feedback):
    global gesture_name
    print ('[Feedback] executing %s'%gesture_name)


rospy.init_node('manip_action_client')

action_server_name = '/manip_action_server'
client = actionlib.SimpleActionClient (action_server_name, manip_action_msgAction)

rospy.loginfo('Waiting for action server' +action_server_name)
client.wait_for_server()
rospy.loginfo('Action server found ...' +action_server_name)

goal = manip_action_msgGoal()
client.send_goal (goal, feedback_cb=feedback_callback)
state_result = client.get_state()
while state_result < DONE:
    gesture_name = raw_input ("Insert gesture name: ")
    goal.goal = gesture_name
    client.send_goal (goal, feedback_cb=feedback_callback)

    if state_result == ERROR:
        rospy.logerr ("Something went wrong Server Side")

    if state_result == WARN:
        rospy.logerr ("There is a warning in the Server side")
