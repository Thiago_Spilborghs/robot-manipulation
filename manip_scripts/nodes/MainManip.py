#!/usr/bin/env python

import rospy
import numpy
import time
from std_msgs.msg import String
import Object_Manip as om
import Gesture_Play as gp

class Manipulator():
    def __init__(self):
        #self.pub = rospy.publisher('/Manipulation',String,queue_size=10)
        rospy.Subscriber('/Manipulation',String,self.callback)
        self.Chosen = ''
        self.ex = None
        self.execute()

    def callback(self,data):
        self.Chosen = data.data

    def execute(self):
        while self.Chosen == '':
            time.sleep(1)
        V_word = self.Chosen.split()
        if V_word[0] == 'coord:':
            gt = om.GoTo(float(V_word[1]),float(V_word[2]),float(V_word[3]))
            self.Chosen == ''
        else:
            g = gp.gest(str(V_word[0]))
            self.Chosen == ''

if __name__ == '__main__':
    rospy.init_node('Manipulation', anonymous=True)
    start = Manipulator()
    try:
        rospy.spin()
    except KeboardInterrupt:
        rospy.loginfo('Shutting down node Manipulator')
