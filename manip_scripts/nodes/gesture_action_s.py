#!/usr/bin/env python

import rospy
import yaml
import os.path
import time
import numpy as np
import actionlib
from std_msgs.msg import Float64
from dynamixel_msgs.msg import JointState
from dynamixel_controllers.srv import TorqueEnable, SetSpeed
from manip_scripts.msg import manip_action_msgAction, manip_action_msgFeedback, manip_action_msgResult, manip_action_msgGoal

position_joint_1 = 0  #base
position_joint_2 = 0  #shoulder
position_joint_3 = 0  #elbow
position_joint_4 = 0  #wrist
position_joint_6 = 0  #grip


def callback_joint_1(data):
    global position_joint_1
    position_joint_1 = data.current_pos


def callback_joint_2(data):
    global position_joint_2
    position_joint_2 = data.current_pos


def callback_joint_3(data):
    global position_joint_3
    position_joint_3 = data.current_pos


def callback_joint_4(data):
    global position_joint_4
    position_joint_4 = data.current_pos


def callback_joint_6(data):
    global position_joint_6
    position_joint_6 = data.current_pos

class ManipActionMsgClass (object):
    _feedback = manip_action_msgFeedback()
    _result = manip_action_msgResult()

    def __init__(self):
        self._as = actionlib.SimpleActionServer("manip_action_server", manip_action_msgAction, self.goal_callback, False)
        self._as.start()

    def goal_callback (self, goal):
        success=True
        r = rospy.Rate(1)

        global position_joint_1, position_joint_2, position_joint_3, position_joint_4, position_joint_6

        # Services
        self.srv_joint_1 = rospy.ServiceProxy('/tilt_controller/torque_enable', TorqueEnable, persistent=True)
        self.srv_joint_2 = rospy.ServiceProxy('/tilt2_controller/torque_enable', TorqueEnable, persistent=True)
        self.srv_joint_3 = rospy.ServiceProxy('/tilt3_controller/torque_enable', TorqueEnable, persistent=True)
        self.srv_joint_4 = rospy.ServiceProxy('/tilt4_controller/torque_enable', TorqueEnable, persistent=True)
        self.srv_joint_6 = rospy.ServiceProxy('/tilt6_controller/torque_enable', TorqueEnable, persistent=True)

        # Subscribers
        rospy.Subscriber('/tilt_controller/state', JointState, callback_joint_1)
        rospy.Subscriber('/tilt2_controller/state', JointState, callback_joint_2)
        rospy.Subscriber('/tilt3_controller/state', JointState, callback_joint_3)
        rospy.Subscriber('/tilt4_controller/state', JointState, callback_joint_4)
        rospy.Subscriber('/tilt6_controller/state', JointState, callback_joint_6)
        #Publishers
        self.joint1 = rospy.Publisher('/tilt_controller/command', Float64, queue_size=10)  # base
        self.joint2 = rospy.Publisher('/tilt2_controller/command', Float64, queue_size=10)  #shoulder
        self.joint3 = rospy.Publisher('/tilt3_controller/command', Float64, queue_size=10)  #elbow
        self.joint4 = rospy.Publisher('/tilt4_controller/command', Float64, queue_size=10)  #wrist
        self.joint6 = rospy.Publisher('/tilt6_controller/command', Float64, queue_size=10)  #grip

        gesture_name = goal.goal


        try:


           self.srv_joint_1(True)
           self.srv_joint_2(True)
           self.srv_joint_3(True)
           self.srv_joint_4(True)
           self.srv_joint_6(True)

           if(gesture_name == "open_grip"):

               self._feedback.feedback = 'Executing'
               self._as.publish_feedback(self._feedback)
               self.joint6.publish(2.0)


           elif(gesture_name == "close_grip"):

               self._feedback.feedback = 'Executing'
               self._as.publish_feedback(self._feedback)
               self.joint6.publish(-1.3)

           elif(gesture_name == "point_foward"):

               self._feedback.feedback = 'Executing'
               self._as.publish_feedback(self._feedback)
               self.joint4.publish(0.5)

           elif(gesture_name == "point_up"):

               self._feedback.feedback = 'Executing'
               self._as.publish_feedback(self._feedback)
               self.joint4.publish(2)


           elif(gesture_name == "elbow_up"):

               self._feedback.feedback = 'Executing'
               self._as.publish_feedback(self._feedback)
               self.joint3.publish(-1.0)


           elif(gesture_name == "reach"):

               self._feedback.feedback = 'Executing'
               self._as.publish_feedback(self._feedback)
               self.joint2.publish(1.7)


           elif(gesture_name == "pull"):

               self._feedback.feedback = 'Executing'
               self._as.publish_feedback(self._feedback)
               self.joint2.publish(0.7)

           elif(gesture_name == "elbow_down"):

               self._feedback.feedback = 'Executing'
               self._as.publish_feedback(self._feedback)
               self.joint3.publish(0.3)


           elif(gesture_name == "swipe_r"):

               self._feedback.feedback = 'Executing'
               self._as.publish_feedback(self._feedback)
               self.joint1.publish(-0.5)

           elif(gesture_name == "swipe_l"):

               self._feedback.feedback = 'Executing'
               self._as.publish_feedback(self._feedback)
               self.joint1.publish(0.5)

           elif(gesture_name == "arm_stretch"):

               self._feedback.feedback = 'Executing'
               self._as.publish_feedback(self._feedback)
               self.joint1.publish(0.0)
               self.joint2.publish(-1.7)
               self.joint3.publish(0.3)
               self.joint4.publish(0.5)

           elif(gesture_name == "arm_stretch_l"):

               self._feedback.feedback = 'Executing'
               self._as.publish_feedback(self._feedback)
               self.joint1.publish(0.5)
               self.joint2.publish(-1.7)
               self.joint3.publish(0.3)
               self.joint4.publish(0.5)
               self.joint6.publish(-1.3)

           elif(gesture_name == "arm_stretch_r"):

               self._feedback.feedback = 'Executing'
               self._as.publish_feedback(self._feedback)
               self.joint1.publish(-0.5)
               self.joint2.publish(-1.7)
               self.joint3.publish(0.3)
               self.joint4.publish(0.5)
               self.joint6.publish(-1.3)

           elif(gesture_name == "pull_back"):

               self._feedback.feedback = 'Executing'
               self._as.publish_feedback(self._feedback)
               self.joint1.publish(0.0)
               self.joint2.publish(0.7)
               self.joint3.publish(-2.4)
               self.joint4.publish(0.5)

        except KeyboardInterrupt:
            rospy.loginfo ("Gesture Cancelled")

        if success:
            self._result = "Gesture Executed"
            rospy.loginfo("Done")
            self._as.set_succeeded(self._result)

if __name__ == '__main__':
    rospy.init_node('gesture_action_server')
    ManipActionMsgClass ()
    rospy.spin()
